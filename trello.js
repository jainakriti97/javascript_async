function getBoard(cb) {
  return setTimeout(function() {
    let board = {
      id: "def453ed",
      name: "Thanos"
    };
    cb(board);
  }, 1000);
}

function getLists(boardId, cb) {
  return setTimeout(function() {
    let lists = {
      def453ed: [
        {
          id: "qwsa221",
          name: "Mind"
        },
        {
          id: "jwkh245",
          name: "Space"
        },
        {
          id: "azxs123",
          name: "Soul"
        },
        {
          id: "cffv432",
          name: "Time"
        },
        {
          id: "ghnb768",
          name: "Power"
        },
        {
          id: "isks839",
          name: "Reality"
        }
      ]
    };
    cb(lists[boardId]);
  }, 1000);
}

function getCards(listId, cb) {
  return setTimeout(function() {
    let cards = {
      qwsa221: [
        {
          id: "ornd494",
          description: `Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar`
        },
        {
          id: "lwpw123",
          description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
        }
      ]
    };
    cb(cards[listId]);
  }, 1000);
}

getBoard(function(board) {
  console.log("board", board);
  getLists(board.id, function(lists) {
    console.log("lists", lists);
    getCards(lists[0].id, function(cards) {
      console.log("cards", cards);
      getChecklists(cards[0].id, function(checklists) {
        console.log("checklists", checklists);
        getCheckListItems(checklists[0].id, function(checklistItems) {
          console.log("checklistItems", checklistItems);
        });
      });
    });
    getCards(lists[1].id, function(cards) {
      console.log("cards", cards);
      getChecklists(cards[0].id, function(checklists) {
        console.log("checklists", checklists);
        getCheckListItems(checklists[0].id, function(checklistItems) {
          console.log("checklistItems", checklistItems);
        });
      });
    });
  });
});

getBoard()
  .then(board => board.id)
  .then(boardId => getLists(boardId))
  .then(lists => lists[0].id)
  .then(listId => getCards(listId))
  .then(cards => cards[0].id);
